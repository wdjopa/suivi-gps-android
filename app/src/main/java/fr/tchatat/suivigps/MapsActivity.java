package fr.tchatat.suivigps;

import androidx.annotation.NonNull;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.FragmentActivity;

import android.Manifest;
import android.content.Context;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.util.Log;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

public class MapsActivity extends FragmentActivity implements OnMapReadyCallback, LocationListener {

    private GoogleMap mMap;
    private MarkerOptions mar = new MarkerOptions();

    private static final int PERMISSION_GPS = 100;
    private LocationManager lm;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_maps);

        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);

        OkHttpClient client = new OkHttpClient();


        Request request = new Request.Builder()
                        .url("http://api.openchargemap.io/v2/poi/?output=json&countrycode=FR&maxresults=20&compact=true&verbose=false&Latitude=49.4431&Longitude=1.0993&distance=30&distanceunit=KM&opendata=true")
                        .build();

        client.newCall(request).enqueue(new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {
//                Toast.makeText(MapsActivity.this, e.getMessage(), Toast.LENGTH_SHORT).show();
                Log.d("MapsActivity", "erreur: "+e.getMessage());
            }

            @Override
            public void onResponse(Call call, Response response) throws IOException {
                final String webContent = response.body().string();

                if(!response.isSuccessful()){
                    Log.d("MapsActivity", "erreur: "+response);
                    Toast.makeText(MapsActivity.this, response.toString(), Toast.LENGTH_SHORT).show();
                    throw new IOException("Erreur : "+response);
                }
                else{
                    Log.d("MapsActivity", webContent);
                    MapsActivity.this.runOnUiThread(()->{
                        JSONArray points = null;
                        try {
                            points = new JSONArray(webContent);
                            Log.d("MapsActivity", points.length()+" Positions");
                            for (int i = 0; i < points.length(); i++) {
                                JSONObject point = points.getJSONObject(i);
                                Log.d("MapsActivity", " Point : "+point);
                                JSONObject infos = point.getJSONObject("AddressInfo");
                                String title = infos.getString("Title");
                                double lat = infos.getDouble("Latitude");
                                double lon = infos.getDouble("Longitude");
                                Toast.makeText(MapsActivity.this, "Ajout d'un nouveau point", Toast.LENGTH_SHORT).show();
                                if(infos.getString("Postcode").substring(0, 5).equals("76000"))
                                    setPointLocation(title, lat, lon);
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    });
                }
            }
        });

        lm = (LocationManager) getSystemService(Context.LOCATION_SERVICE);

        if(ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED){
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, PERMISSION_GPS);
        }
        else{
            lm.requestLocationUpdates(LocationManager.GPS_PROVIDER, 0, 0, (LocationListener) MapsActivity.this);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        if(requestCode == PERMISSION_GPS){
            if(ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED)
                lm.requestLocationUpdates(LocationManager.GPS_PROVIDER, 0, 0, (LocationListener) MapsActivity.this);
            else
                Toast.makeText(MapsActivity.this, "Permission refusée", Toast.LENGTH_SHORT).show();
        }
    }

    private void setPointLocation(String title, Double lat, Double lon){
        Log.d("MapsActivity", " Titre : "+title+" ("+lon+", "+lat+")");

        LatLng point = new LatLng(lat, lon);
        mMap.addMarker(new MarkerOptions().position(point ).title(title));
        mMap.moveCamera(CameraUpdateFactory.newLatLng(point));

    }


    /**
     * Manipulates the map once available.
     * This callback is triggered when the map is ready to be used.
     * This is where we can add markers or lines, add listeners or move the camera. In this case,
     * we just add a marker near Sydney, Australia.
     * If Google Play services is not installed on the device, the user will be prompted to install
     * it inside the SupportMapFragment. This method will only be triggered once the user has
     * installed Google Play services and returned to the app.
     */
    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;

        mMap.setMyLocationEnabled(true);
        //LatLng sydney = new LatLng(-34, 151);
        //mMap.addMarker(new MarkerOptions().position(sydney).title("Marker in Sydney"));
        //mMap.moveCamera(CameraUpdateFactory.newLatLng(sydney));
    }

    @Override
    public void onLocationChanged(Location location) {
        LatLng pos = new LatLng(location.getLatitude(), location.getLongitude());
        mar.position(pos).title("Parcours");
        float zoom = 16.0f;
        mMap.moveCamera(CameraUpdateFactory.newLatLng(pos));
    }

    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {

    }

    @Override
    public void onProviderEnabled(String provider) {

    }

    @Override
    public void onProviderDisabled(String provider) {

    }
}
